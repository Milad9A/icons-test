import 'package:flutter/material.dart';
import 'package:icons_test/screens/home_screen.dart';

void main() {
  runApp(IconsTestApp());
}

class IconsTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}
