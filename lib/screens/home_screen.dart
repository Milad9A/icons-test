import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        children: [
          Image.asset('assets/icons/add_to_cart_store_screen.png'),
          Image.asset('assets/icons/address_my_ooddss_screen.png'),
          Image.asset('assets/icons/all_products_home_screen.png'),
          Image.asset('assets/icons/baby_category_home_screen.png'),
          Image.asset('assets/icons/billing_address_orders_details_screen.png'),
          Image.asset('assets/icons/books_category_home_screen.png'),
          Image.asset('assets/icons/buyer_card_money.png'),
          Image.asset('assets/icons/calendar_advanced_search.png'),
          Image.asset('assets/icons/camera_edit_store_screen.png'),
        ],
      ),
    );
  }
}
